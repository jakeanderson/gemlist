# Gemlist
A curated list of Ruby gems and resources I've used or plan to use in projects.

## Admin Interfaces
- [Administrate](https://github.com/thoughtbot/administrate) - A Rails engine that helps you put together a super-flexible admin dashboard, by Thoughtbot.

## API Builders
- [jbuilder](https://github.com/rails/jbuilder) - Create JSON structures via a Builder-style DSL.
- [Fast JSON API](https://github.com/Netflix/fast_jsonapi) - A lightning fast JSON:API serializer for Ruby Objects.

## Authentication
- [Devise](https://github.com/plataformatec/devise) - A flexible authentication solution for Rails based on Warden.
- [Knock](https://github.com/nsarno/knock) - Seamless JWT authentication for Rails API.

## Authorization
- [pretender](https://github.com/ankane/pretender) - Log in as another user in Rails
- [Pundit](https://github.com/varvet/pundit) - Minimal authorization through OO design and pure Ruby classes.
- [rolify](https://github.com/RolifyCommunity/rolify) - Role management library with resource scoping

## Caching
- [Dalli](https://github.com/petergoldstein/dalli) - A high performance pure Ruby client for accessing memcached servers.

## Code Analysis & Metrics
- [Brakeman](https://github.com/presidentbeef/brakeman) - A static analysis security vulnerability scanner for Ruby on Rails applications.
- [RuboCop](https://github.com/rubocop-hq/rubocop) - A static code analyzer, based on the community Ruby style guide.
- [Rubycritic](https://github.com/whitesmith/rubycritic) - A Ruby code quality reporter.
- [SimpleCov](https://github.com/colszowka/simplecov) - Code coverage for Ruby 1.9+ with a powerful configuration library and automatic merging of coverage across test suites.
- [Traceroute](https://github.com/amatsuda/traceroute) - A Rake task gem that helps you find the dead routes and actions for your Rails 3+ app

## Code Structure
- [Draper](https://github.com/drapergem/draper) - Draper adds an object-oriented layer of presentation logic to your Rails application.
- [guides](https://github.com/thoughtbot/guides) - A guide for programming in style.
- [trailblazer](https://github.com/trailblazer/trailblazer) - A High-Level Architecture for Ruby.

## Concurrency & Parallelism
- [Concurrent Ruby](https://github.com/ruby-concurrency/concurrent-ruby)- Modern concurrency tools including agents, futures, promises, thread pools, supervisors, and more. Inspired by Erlang, Clojure, Scala, Go, Java, JavaScript, and classic concurrency patterns.
- [EventMachine](https://github.com/eventmachine/eventmachine) - An event-driven I/O and lightweight concurrency library for Ruby.

## Configuration
- [dotenv](https://github.com/bkeepers/dotenv) - Loads environment variables from .env.
- [Figaro](https://github.com/laserlemon/figaro) - Simple, Heroku-friendly Rails app configuration using ENV and a single YAML file.

## Content Management Systems (CMS)
- [Spina CMS](https://github.com/SpinaCMS/Spina) - A beautiful CMS for Rails developers.

## Core Extensions
- [ActiveSupport](https://github.com/rails/rails/tree/master/activesupport) - A collection of utility classes and standard library extensions.
- [Hamster](https://github.com/hamstergem/hamster) - Efficient, immutable, and thread-safe collection classes for Ruby.

## Dashboards, Data Processing, & Data Visualization
- [Blazer](https://github.com/ankane/blazer) - Simple data viewer using only SQL. Output to table, chart, and maps.
- [Dashing-Rails](https://github.com/gottfrois/dashing-rails) - The exceptionally handsome dashboard framework for Rails.
- [Kiba](https://github.com/thbar/kiba/) - A lightweight data processing / ETL framework for Ruby.
- [Chartkick](https://chartkick.com/) - Create beautiful Javascript charts with one line of Ruby. Works with Rails, Sinatra and most browsers (including IE 6).
- [axlsx](https://github.com/randym/axlsx) - xlsx generation with charts, images, automated column width, customizable styles and full schema validation. Axlsx excels at helping you generate beautiful Office Open XML Spreadsheet documents without having to understand the entire ECMA specification. 

## Database Drivers & Tools
- [activerecord-typedstore](https://github.com/byroot/activerecord-typedstore) - ActiveRecord::Store but with type definition
- [amoeba](https://github.com/amoeba-rb/amoeba) - A ruby gem to allow the copying of ActiveRecord objects and their associated children, configurable with a DSL on the model
- [mongo-ruby-driver](https://github.com/mongodb/mongo-ruby-driver) - MongoDB Ruby driver.
- [redis-rb](https://github.com/redis/redis-rb) - A Ruby client that tries to match Redis' API one-to-one, while still providing an idiomatic interface.
- [ruby-pg](https://bitbucket.org/ged/ruby-pg/wiki/Home) - Ruby interface to PostgreSQL 8.3 and later.
- [Database Cleaner](https://github.com/DatabaseCleaner/database_cleaner) - Database Cleaner is a set of strategies for cleaning your database in Ruby.
- [PgHero](https://github.com/ankane/pghero) - Postgres insights made easy.
- [Seedbank](https://github.com/james2m/seedbank) - Seedbank allows you to structure your Rails seed data instead of having it all dumped into one large file.

## Date & Time Processing
- [business_time](https://github.com/bokmann/business_time) - Support for doing time math in business hours and days.
- [ByStar](https://github.com/radar/by_star) - Find ActiveRecord objects by year, month, fortnight, week and more!
- [Chronic](https://github.com/mojombo/chronic) - A natural language date/time parser written in pure Ruby.
- [groupdate](https://github.com/ankane/groupdate) - The simplest way to group temporal data in ActiveRecord, arrays and hashes.
- [ice_cube](https://github.com/seejohnrun/ice_cube) - A date recurrence library which allows easy creation of recurrence rules and fast querying.
- [local_time](https://github.com/basecamp/local_time) - Rails Engine for cache-friendly, client-side local time.
- [timezone](https://github.com/panthomakos/timezone) - Accurate current and historical timezones for Ruby with support for Geonames and Google latitude - longitude lookups.

## Debugging
- [Byebug](https://github.com/deivid-rodriguez/byebug) - A simple to use, feature rich debugger for Ruby 2.
- [Pry Byebug](https://github.com/deivid-rodriguez/pry-byebug) - Pry navigation commands via byebug.

## DevOps Tools
- [Capistrano](https://github.com/capistrano/capistrano) - A remote server automation and deployment tool written in Ruby.
- [Chef](https://github.com/chef/chef) - A systems integration framework, built to bring the benefits of configuration management to your entire infrastructure.
	- I'd prefer Ansible over Chef, though Ansible is not a Ruby gem
- [Mina](https://github.com/mina-deploy/mina) - Really fast deployer and server automation tool.
- [Overcommit](https://github.com/brigade/overcommit) - A fully configurable and extendable Git hook manager.
- [parity](https://github.com/thoughtbot/parity) - Shell commands for development, staging, and production parity for Heroku apps

## Documentation
- [Annotate](https://github.com/ctran/annotate_models) - Add a comment documenting the current schema to the top or bottom of each of your ActiveRecord models.
- [Apipie](https://github.com/Apipie/apipie-rails) - Rails API documentation and display tool using Ruby syntax.
- [YARD](https://github.com/lsegal/yard) - YARD enables the user to generate consistent, usable documentation that can be exported to a number of formats very easily.

## E-Commerce & Payments
- [Active Merchant](https://github.com/activemerchant/active_merchant) - A simple payment abstraction library extracted from Shopify.
- [Money](https://github.com/RubyMoney/money) - A Ruby Library for dealing with money and currency conversion.
- [Solidus](https://github.com/solidusio/solidus) - An open source, eCommerce application for high volume retailers.
- [stripe-ruby](https://github.com/stripe/stripe-ruby) - Stripe Ruby bindings.

## Email
- [griddler](https://github.com/thoughtbot/griddler) - Simplify receiving email in Rails  
- [LetterOpener](https://github.com/ryanb/letter_opener) - Preview mail in the browser instead of sending.
- [Postal](https://github.com/atech/postal) - A fully featured open source mail delivery platform for incoming & outgoing e-mail.
- [premailer-rails](https://github.com/fphilipe/premailer-rails) - CSS styled emails without the hassle.

## Error Handling
- [Errbit](https://github.com/errbit/errbit) - The open source, self-hosted error catcher.
- [Rollbar](https://github.com/rollbar/rollbar-gem) - Exception tracking and logging from Ruby to Rollbar


## Feature/AB Testing
- [flipper](https://github.com/jnunemaker/flipper) - Feature flipping for ANYTHING. Make turning features on/off so easy that everyone does it. Whatever your data store, throughput, or experience.
- [Split](https://github.com/splitrb/split) - Rack Based AB testing framework.

## File Uploads
- [Shrine](https://github.com/shrinerb/shrine) - Toolkit for handling file uploads in Ruby.

## Forms / Form Helpers
- [Cocoon](https://github.com/nathanvda/cocoon) - Dynamic nested forms using jQuery made easy; works with formtastic, simple_form or default forms.
- [Simple Form](https://github.com/plataformatec/simple_form) - Rails forms made easy.

## Geolocation
- [Geocoder](https://github.com/alexreisner/geocoder) - A complete geocoding solution for Ruby. With Rails it adds geocoding (by street or IP address), reverse geocoding (find street address based on given coordinates), and distance queries.

## GraphQL
- [graphql-batch](https://github.com/Shopify/graphql-batch) – A query batching executor.
- [graphql-client ](https://github.com/github/graphql-client)- A library for declaring, composing and executing GraphQL queries.
- [graphql-guard](https://github.com/exAspArk/graphql-guard) - A simple field-level authorization.
- [graphql-ruby](https://github.com/rmosolgo/graphql-ruby) - Ruby implementation of GraphQL.

## HTTP Requests
- [Faraday](https://github.com/lostisland/faraday) - an HTTP client lib that provides a common interface over many adapters (such as Net::HTTP) and embraces the concept of Rack middleware when processing the request/response cycle.
- [httparty](https://github.com/jnunemaker/httparty) - Makes http fun again!

## Image/File Processing
- [MiniMagick](https://github.com/minimagick/minimagick) - A ruby wrapper for ImageMagick or GraphicsMagick command line.
- [Pdfkit](https://github.com/pdfkit/pdfkit) - HTML+CSS to PDF using wkhtmltopdf.
- [Prawn](https://github.com/prawnpdf/prawn) - Fast, Nimble PDF Writer for Ruby.
- [Wicked Pdf](https://github.com/mileszs/wicked_pdf) - PDF generator (from HTML) plugin for Ruby on Rails.

## Implementations/Compilers/Runtimes
- [JRuby](https://github.com/jruby/jruby) - A Java implementation of the Ruby language.
- [Rubinius](https://github.com/rubinius/rubinius) - An implementation of the Ruby programming language. Rubinius includes a bytecode virtual machine, Ruby syntax parser, bytecode compiler, generational garbage collector, just-in-time (JIT) native machine code compiler, and Ruby Core and Standard libraries.
- [TruffleRuby](https://github.com/oracle/truffleruby) - A high performance implementation of the Ruby programming language. Built on the GraalVM by Oracle Labs.

## Logging
- [Lograge](https://github.com/roidrage/lograge) - An attempt to tame Rails' default policy to log everything.
- [MongoDB Logger](https://github.com/le0pard/mongodb_logger) - MongoDB logger for Rails.
- [oink](https://github.com/noahd1/oink) - Log parser to identify actions which significantly increase VM heap size

## Machine Learning**
- [Awesome Machine Learning with Ruby](https://github.com/arbox/machine-learning-with-ruby) - A Curated List of Ruby Machine Learning Links and Resources.
- [decisiontree](https://github.com/igrigorik/decisiontree) - ID3-based implementation of the ML Decision Tree algorithm 

## Mobile Development
- [RubyMotion](http://www.rubymotion.com/) - A revolutionary toolchain that lets you quickly develop and test full-fledged native iOS and OS X applications for iPhone, iPad, Mac and Android.

## Navigational Helpers
- [active_link_to](https://github.com/comfy/active_link_to) - View helper to manage "active" state of a link.
- [Gretel](https://github.com/lassebunk/gretel) - A Ruby on Rails plugin that makes it easy yet flexible to create breadcrumbs.

## ORM/ODM
- [ActiveRecord](https://github.com/rails/rails/tree/master/activerecord) - Object-relational mapping in Rails.
- [Mongoid](https://github.com/mongodb/mongoid) - An ODM (Object-Document-Mapper) framework for MongoDB in Ruby.
- [Sequel](https://github.com/jeremyevans/sequel) - Sequel is a simple, flexible, and powerful SQL database access toolkit for Ruby.

## ORM/Active Record Helpers
- [ActiveRecord Import](https://github.com/zdennis/activerecord-import) - a library for bulk inserting data using ActiveRecord.
- [ActsAsList](https://github.com/swanandp/acts_as_list) - Provides the capabilities for sorting and reordering a number of objects in a list.
- [ActiveValidators](https://github.com/franckverrot/activevalidators) - An exhaustive collection of off-the-shelf and tested ActiveModel/ActiveRecord validations.
- [Ancestry](https://github.com/stefankroes/ancestry) - Organise ActiveRecord model into a tree structure using a variation on the materialised path pattern.
- [Apartment](https://github.com/influitive/apartment) - Multi-tenancy for Rails and ActiveRecord.
- [Audited](https://github.com/collectiveidea/audited) - Audited is an ORM extension for ActiveRecord & MongoMapper that logs all changes to your models.
- [DeepPluck](https://github.com/khiav223577/deep_pluck) - Allow you to pluck attributes from nested associations without loading a bunch of records.
- [Discard](https://github.com/jhawthorn/discard) - Soft deletes for ActiveRecord done right.
- [Merit](https://github.com/merit-gem/merit) - Adds reputation behavior to Rails apps in the form of Badges, Points, and Rankings for ActiveRecord or Mongoid.
- [PaperTrail](https://github.com/paper-trail-gem/paper_trail) - Track changes to your ActiveRecord models' data for auditing or versioning.
- [ranked-model](https://github.com/mixonic/ranked-model) - A modern row sorting library for ActiveRecord. It uses ARel aggressively and is better optimized than most other libraries.
- [Unread](https://github.com/ledermann/unread) - Manage read/unread status of ActiveRecord objects - and it's fast.

## Package Management
- [Foreman](https://github.com/ddollar/foreman) - Manage Procfile-based applications.
- [Hivemind](https://github.com/DarthSim/hivemind) - Process manager for Procfile-based applications
	- Technically not a ruby gem, but much faster/better than Foreman for managing processes since it's written in Go
- [Overmind](https://github.com/DarthSim/overmind) - Process manager for Procfile-based applications and tmux
- [Homebrew](https://github.com/Homebrew/brew) - The missing package manager for OS X.

## Pagination
- [Kaminari](https://github.com/kaminari/kaminari) - A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for modern web app frameworks and ORMs.
- [Pagy](https://github.com/ddnexus/pagy) - Pagy is the ultimate pagination gem that outperforms the others in each and every benchmark and comparison. More details can be found on Pagy Wiki.

## Performance Monitoring
- [New Relic](https://github.com/newrelic/rpm) - Find and fix Ruby errors with New Relic application monitoring and troubleshooting.
- [Scout](https://github.com/scoutapp/scout_apm_ruby) - Scout Ruby Application Monitoring Agent.

## Profiling & Optimization
- [bullet](https://github.com/flyerhzm/bullet) - Help to kill N+1 queries and unused eager loading.
- [rack-mini-profiler](https://github.com/MiniProfiler/rack-mini-profiler) - Profiler for your development and production Ruby rack apps.

## Queues & Messaging
- [Bunny](https://github.com/ruby-amqp/bunny) - Bunny is a popular, easy to use, well-maintained Ruby client for RabbitMQ (3.3+).
- [Karafka](https://github.com/karafka/karafka) - Framework used to simplify Apache Kafka (a distributed streaming platform) based Ruby applications development.
- [Resque](https://github.com/resque/resque) - A Redis-backed Ruby library for creating background jobs.
- [Sidekiq](https://github.com/mperham/sidekiq/) - A full-featured background processing framework for Ruby. It aims to be simple to integrate with any modern Rails application and much higher performance than other existing solutions.
- [Sidekiq Cron](https://github.com/ondrejbartas/sidekiq-cron) - Scheduler / Cron for Sidekiq jobs

## Search
- [pg_search](https://github.com/Casecommons/pg_search) - Builds ActiveRecord named scopes that take advantage of PostgreSQL's full text search.
- [ransack](https://github.com/activerecord-hackery/ransack/) - Object-based searching.
- [Searchkick](https://github.com/ankane/searchkick) - Searchkick learns what your users are looking for. As more people search, it gets smarter and the results get better. It’s friendly for developers - and magical for your users.

## Security
- [Rack::Attack](https://github.com/kickstarter/rack-attack) - Rack middleware for blocking & throttling abusive requests.
- [Rails Security Checklist](https://github.com/eliotsykes/rails-security-checklist) - Community-driven Rails Security Checklist
- [SecureHeaders](https://github.com/twitter/secure_headers) - Automatically apply several headers that are related to security, including: Content Security Policy (CSP), HTTP Strict Transport Security (HSTS), X-Frame-Options (XFO), X-XSS-Protection, X-Content-Type-Options, X-Download-Options & X-Permitted-Cross-Domain-Policies.

## SEO
- [FriendlyId](https://github.com/norman/friendly_id) - The "Swiss Army bulldozer" of slugging and permalink plugins for Active Record.
- [MetaTags](https://github.com/kpumuk/meta-tags) - A gem to make your Rails application SEO-friendly.

## State Machines
- [AASM](https://github.com/aasm/aasm) - State machines for Ruby classes (plain Ruby, Rails Active Record, Mongoid).

## Testing
- [RSpec](https://github.com/rspec/rspec) - Behaviour Driven Development for Ruby.
- [Capybara](https://github.com/teamcapybara/capybara) - Acceptance test framework for web applications.
- [minitest](https://github.com/seattlerb/minitest) - minitest provides a complete suite of testing facilities supporting TDD, BDD, mocking, and benchmarking.
- [shoulda-matchers](https://github.com/thoughtbot/shoulda-matchers) - Provides Test::Unit- and RSpec-compatible one-liners that test common Rails functionality. These tests would otherwise be much longer, more complex, and error-prone.
- [factory_bot](https://github.com/thoughtbot/factory_bot) - A library for setting up Ruby objects as test data.
- [faker](https://github.com/stympy/faker) - A library for generating fake data such as names, addresses, and phone numbers.
- [WebMock](https://github.com/bblimke/webmock) - Library for stubbing and setting expectations on HTTP requests.
- [Parallel Tests](https://github.com/grosser/parallel_tests) - Speedup Test::Unit + RSpec + Cucumber by running parallel on multiple CPUs (or cores).
- [test-prof](https://github.com/palkan/test-prof) - Ruby Tests Profiling Toolbox
- [timecop](https://github.com/travisjeffery/timecop) - Provides "time travel" and "time freezing" capabilities, making it dead simple to test time-dependent code.

## Third Party API Libraries
- [aws](https://github.com/aws/aws-sdk-ruby) - The official AWS SDK for Ruby. 
- [Dropbox](https://github.com/Jesus/dropbox_api) - Ruby client for Dropbox API v2.
- [droplet-kit](https://github.com/digitalocean/droplet_kit) - DropletKit is the official DigitalOcean API client for Ruby.
- [gitlab](https://github.com/NARKOZ/gitlab) - Ruby wrapper and CLI for the GitLab API.
- [gmail](https://github.com/gmailgem/gmail) - A Rubyesque interface to Gmail, with all the tools you'll need.
- [google-cloud-ruby](https://github.com/googleapis/google-cloud-ruby) - Google Cloud Client Library for Ruby
- [instagram-ruby-gem](https://github.com/facebookarchive/instagram-ruby-gem) - The official gem for the Instagram REST and Search APIs.
- [linkedin](https://github.com/hexgnu/linkedin) - Provides an easy-to-use wrapper for LinkedIn's REST APIs.
- [Octokit](https://github.com/octokit/octokit.rb) - Ruby toolkit for the GitHub API.
- [Pusher](https://github.com/pusher/pusher-http-ruby) - Ruby server library for the Pusher API.
- [Quickbooks](https://github.com/ruckus/quickbooks-ruby) - Quickbooks Online REST API V3 - Ruby
- [ruby-trello](https://github.com/jeremytregunna/ruby-trello) - Implementation of the Trello API for Ruby.
- [Slack Notifier](https://github.com/stevenosloan/slack-notifier) - A simple wrapper for posting to Slack channels.
- [twilio-ruby](https://github.com/twilio/twilio-ruby) - A module for using the Twilio REST API and generating valid TwiML.
- [twitter](https://github.com/sferik/twitter) - A Ruby interface to the Twitter API.
- [Yt](https://github.com/Fullscreen/yt) - An object-oriented Ruby client for YouTube API V3.
